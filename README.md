# Wardrobify

Team:

- Person 1 - Which microservice?
- Person 2 - Which microservice?

## Design

Wardrobe API: provides Location and Bin RESTful API endpoints
Database: the PostgreSQL database that will hold the data of all of the microservices
React: the React-based front-end application where both you and your teammate will write the components to interact with your services
Shoes API: the RESTful API to interact with Shoe resources
Shoes Poller: the poller to poll the Wardrobe API for Bin resources
Hats API: the RESTful API to interact with Hat resources
Hats Poller: the poller to poll the Wardrobe API for Location resources

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

I will create a hats model that will have the properties of its fabric, style name, color, and URL for a picture, as well as the location in the wardrobe where it exists.
Using that hats model, I will create api view functions that will get a list of hats, create a new hat, and delete a hat.
In the React folder, I will create hat components to show a list of all the hats and their details. I will also create React components to show a form to create a new hat, as well as a way to delete a hat.
All of these will route the existing navigation links to the components.
I will also implement a poling application that uses the Django resources to pull Location data form the Wardrobe API.

Explain your models and integration with the wardrobe
microservice, here.
