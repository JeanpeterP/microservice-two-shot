import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';
import HatsList from "./HatsList";
import HatForm from "./HatForm";

export default function App(props) {
  if (props.shoes === undefined) {
    return null;
  }
  return (
    <>
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes/new" element={<ShoeForm />} />
          <Route path="/shoes" element={<ShoeList shoes={props.shoes}/>} />
          <Route path="hats">
						<Route path="new" element={<HatForm />} />
					</Route>
        </Routes>
      </div>
    </BrowserRouter>
    </>
  );
}