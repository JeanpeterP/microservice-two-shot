import React, {useState, useEffect} from 'react';

export default function ShoeForm() {
    const [bins, setBins] = useState([]);
    const [formData, setFormData] = useState({
        name: '',
        manufacturer: '',
        color: '',
        size: '',
        bin: '',
    })
    const handleSubmit = async (event) => {
        event.preventDefault();

        const shoeUrl = 'http://localhost:8080/api/shoes/'

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json()
            console.log(newShoe)
            setFormData({
                name: '',
                manufacturer: '',
                color: '',
                size: '',
                bin: '',
            })
        } else {
            console.log("ERROR LOADING URL RESPONSE")
        }
    }

    const fetchData = async () => {
        const url = "http://localhost:8100/api/bins/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data.bins)
            setBins(data.bins)
        } else {
            console.log("ERROR LOADING URL")
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Shoe</h1>
            <form  onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input  placeholder="Name" required type="text" name="name" id="name" className="form-control"  />
                <label htmlFor="name">Model</label>
              </div>
              <div className="form-floating mb-3">
                <input  placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                <label htmlFor="room_count">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input  placeholder="Color" required type="text" id="color" name="color" className="form-control" />
                <label htmlFor="city">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input  placeholder="Size" required type="number" id="size" name="size" className="form-control" />
                <label htmlFor="size">Size</label>
              </div>
              <div className="mb-3">
                <select  required id="state" name="state" className="form-select" >
                  <option value="">Choose a bin</option>
                  <>
                    {bins.map(bin => {
                        return (
                            <option key={bin.href} value={bin.href}>
                                {bin.closet_name}
                            </option>
                        );
                    })}
                  </>
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}