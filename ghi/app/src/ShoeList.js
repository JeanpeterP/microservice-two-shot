import React, {useEffect, useState } from 'react';



export default function ShoeList(props) {
  const [shoes, setShoes] = useState(props.shoes)

  const handleDelete = async (shoe) => {

    const deleteShoeUrl = `http://localhost:8080${shoe.href}`

    const response = await fetch(deleteShoeUrl, {
      method: 'DELETE',
    });
    if (response.ok){
      setShoes(shoes.filter(s => s.pk !== shoe.pk));
      console.log(`Shoe with id ${shoe} has been deleted`);
    } else {
      console.log("Error deleting shoe")
    }
  }


  return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Brand</th>
            <th>Model</th>
            <th>Size</th>
            <th>BIN</th>
          </tr>
        </thead>
        <tbody>
          {props.shoes.map((shoe, index) => {
            return (
              
              <tr key={index}>
                <td>{ shoe.manufacturer }</td>
                <td>{ shoe.name }</td>
                <td>{ shoe.size }</td>
                <td>{ shoe.bin }</td>
                <td>
                  <ul className="list-inline m-0">
                    <li className="list-inline-item">
                      <button className="btn btn-primary btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Add"><i className="fas fa-table"></i>Add</button>
                    </li>
                    <li className="list-inline-item">
                      <button onClick={() => handleDelete(shoe)}className="btn btn-danger btn-sm rounded-0" type="button" data-toggle="tooltip" data-placement="top" title="Delete"><i className="fas fa-trash"></i>Delete</button>
                    </li>
                  </ul>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }