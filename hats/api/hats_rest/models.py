from django.db import models
# from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse

# Create your models here.
class LocationVO(models.Model):
  import_href=models.CharField(max_length=200, unique=True)
  closet_name = models.CharField(max_length=200)


class Hats(models.Model):
  # denim, felt, wool, fur, straw, buckram, leather, Twill, Mesh
  fabric =models.CharField(max_length=200)
  style=models.CharField(max_length=200)
  color=models.CharField(max_length=200)
  url = models.URLField(null=True)

  location = models.ForeignKey(
    LocationVO,
    related_name="hats",
    on_delete=models.CASCADE
  )

  def __str__(self):
      return self.name

  def get_api_url(self):
    return reverse("api_show_hat",kwargs={"pk":self.pk})



# class BinVO(models.Model):
