import json
from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import LocationVO, Hats



class LocationVODetailEncoder(ModelEncoder):
  model = LocationVO
  properties = ["closet_name", "import_href"]


class HatsListEncoder(ModelEncoder):
  model = Hats
  properties = ["fabric","style","color","location"]
  encoders={
    "location":LocationVODetailEncoder()
  }

  # def get_extra_data(self,o):
  #   return {"location":o.location.name}


class HatsDetailEncoder(ModelEncoder):
  model = Hats
  properties = ["fabric","style","color","location"]
  encoders={
    "location":LocationVODetailEncoder()
  }



# Create your views here.
@require_http_methods(["GET","POST"])
def api_list_hats(request, location_vo_id=None):
  if request.method == "GET":
    if location_vo_id is not None:
      hats=Hats.objects.filter(location=location_vo_id)
    else:
      hats = Hats.objects.all()

    return JsonResponse(
      {"hats":hats},
      encoder=HatsListEncoder
    )
  else:
    content = json.loads(request.body)
    # print(content)
    try:
      # location_href= f"/api/locations/{location_vo_id}/"
      location=LocationVO.objects.get(import_href=content["location"])
      content["location"]=location
    except LocationVO.DoesNotExist:
      return JsonResponse(
        {"error":"location not found"},
        status=400,
      )

    hat= Hats.objects.create(**content)
    print(hat.__dict__)
    return JsonResponse(
      hat,
      encoder=HatsDetailEncoder,
      safe=False
    )

@require_http_methods(["GET","DELETE","PUT"])
def api_show_hat(request, pk):
  if request.method == "GET":
    hat = Hats.objects.get(id=pk)
    return JsonResponse(
      hat,
      encoder=HatsDetailEncoder,
      safe=False,
      )
  elif request.method == "DELETE":
    count, _ = Hats.objects.filter(id=pk).delete()
    return JsonResponse({"deleted":count > 0})

  # This code below is only needed for a put request

  # else:
  #   content = json.loads(request.body)
  #   print(content)
  #   try:
  #     if "location" in content:
  #       location = LocationVO.objects.get(id=content["location"])
  #   except LocationVO.DoesNotExist:
  #     return JsonResponse(
  #       {"error":"location not found"},
  #       status=400
  #     )
  #   LocationVO.objects.filter(id=pk).update(**content)
  #   location =LocationVO.objects.get(id=pk)
  #   return JsonResponse(
  #     location,
  #     encoder=LocationVODetailEncoder,
  #     safe = False
  #   )
